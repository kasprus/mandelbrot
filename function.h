#ifndef FUNCTION_H
#define FUNCTION_H
#define RES_X 1360
#define RES_Y 680
#define NUMBER_OF_ITERATIONS 2000

void my_function(unsigned char *buffer, int x, int y, double scale, double start_x, double start_y);
// void cudaFunction(unsigned char *buffer, int x, int y, double scale, double start_x, double start_y);
#endif
