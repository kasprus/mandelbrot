NVIDIACC=nvcc
GNUCC=g++-7
LDFLAGS=-L/usr/lib -lallegro -lcuda -lcudart -lstdc++
INCLUDE=-I. -I/usr/include/allegro5

all: main.o function.o
	$(GNUCC) -o program function.o main.o $(INCLUDE) $(LDFLAGS)
	rm *.o

function.o: function.cu cudaHeader.h
	$(NVIDIACC) -c function.cu
main.o: main.cpp function.h
	$(GNUCC) -c main.cpp $(INCLUDE) $(LDFLAGS)

#function.o: function.o
#	$(CC) -c function.c

clean:
	rm -f *.o
