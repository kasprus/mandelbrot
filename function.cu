#include <cuda.h>
#include <iostream>
#include "function.h"
#include "cudaHeader.h"

void my_function(unsigned char *buffer, int x, int y, double scale, double start_x, double start_y) {
  unsigned char *cudaBuffer;
  int blockSize = 1024;
  cudaMallocManaged(&cudaBuffer, 3 * x * y * sizeof(unsigned char));
  cudaFunction<<<(x * y + blockSize - 1) / blockSize, blockSize>>>(cudaBuffer, x, y, scale, start_x, start_y);
  cudaMemcpy((void *)buffer, (const void *)cudaBuffer, 3 * x* y * sizeof(unsigned char), cudaMemcpyDeviceToHost);
  cudaFree(cudaBuffer);
  std::cout<<"dupa\n";
}

__global__ void cudaFunction(unsigned char *buffer, int x, int y, double scale, double start_x, double start_y) {
  int id = blockIdx.x*blockDim.x+threadIdx.x;
  int iterations = NUMBER_OF_ITERATIONS;
  if(id >= x * y)return;
  double x_ = start_x + scale * (id % x);
  double y_ = start_y + scale * (id / x);
  double pomX = x_, pomY = y_;
  double pomZ;
  while(iterations > 0 && x_ * x_ + y_ * y_ <= 4.0) {
    pomZ = x_;
    x_ = pomX + x_ * x_ - y_ * y_;
    y_ = pomY + 2 * y_ * pomZ;
    --iterations;
  }
  id *= 3;
  if(iterations == 0) {
    buffer[id] = buffer[id + 1] = buffer[id + 2] = 0;
  } else {
    buffer[id] = 255 - (iterations) % 255;
    buffer[id + 1] = 255 - (iterations) % 255;
    buffer[id + 2] = 255;
  }
}
